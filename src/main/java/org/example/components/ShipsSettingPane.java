package org.example.components;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import org.example.App;
import org.example.models.Cell;
import org.example.models.UserPane;
import org.example.models.SetShipActionModel;

public class ShipsSettingPane extends GridPane {

    private UserPane userPane = UserPane.getInstance();
    private final ObjectProperty<SetShipActionModel> selectedShip = new SimpleObjectProperty<>();

    public ShipsSettingPane() {

        this.setStyle("-fx-background-color: #313640");

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.add(createCell(i, j), i, j);
            }
        }

        this.rerenderCells();

        this.setOnDragOver(this::handleDragOver);
    }

    public ObjectProperty<SetShipActionModel> selectedShipProperty() {
        return selectedShip;
    }

    @Override
    public ObservableList<Node> getChildren() {
        return super.getChildren();
    }

    private void handleDragOver(DragEvent event) {
        if (event.getGestureSource() != event.getSource() && event.getDragboard().hasString() ) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    private Button createCell(int column, int row) {
        Button button = new Button();
        button.getStyleClass().add("cell");


        button.setOnDragDropped(event -> handleDragDropped(event, row, column));

        return button;
    }

    private void handleDragDropped(DragEvent event, int row, int column) {
        Dragboard db = event.getDragboard();
        boolean completed = false;

        if ( db.hasString() ) {
            int content = Integer.parseInt(db.getString());
            selectedShip.set(new SetShipActionModel(content, row, column));
            rerenderCells();
            completed = true;
        }

        event.setDropCompleted(completed);
        event.consume();
    }

    private void rerenderCells() {
        Cell[][] cellsList = userPane.getCells();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(cellsList[i][j].getShipInfoPointer() != null) {
                    this.getChildren().get(j * 10 + i).getStyleClass().add("ship");
                }
            }
        }
    }
}
