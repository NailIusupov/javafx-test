package org.example.components;

import javafx.application.Platform;
import javafx.beans.NamedArg;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import org.example.App;
import org.example.models.*;
import org.example.models.ShipInfo;

public class BattlePane extends GridPane {

    private Pane gamePane;
    private boolean isUserPane;
    private BattlePane userPane;

    public BattlePane(
            @NamedArg("isUserPane") boolean isUserPane
    ) {

        this.setStyle("-fx-background-color: #313640");
        this.getStylesheets().add(App.class.getResource("styles/game.css").toString());

        this.isUserPane = isUserPane;

        if (this.isUserPane) {
            this.gamePane = UserPane.getInstance();
        } else {
            this.gamePane = new BotPane();
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.add(createCell(i, j), i, j);
            }
        }

        this.rerenderCells();
    }

    public void setUserPane(BattlePane userPane) {
        this.userPane = userPane;
    }

    protected Button createCell(int column, int row) {
        Button button = new Button();
        button.getStyleClass().add("cell");

        if (!this.isUserPane) {
            button.setOnMouseClicked(event -> handleClick(column, row));
        }

        return button;
    }

    private void handleClick(int column, int row) {
        Cell cell = this.gamePane.getCells()[row][column];

        if (cell.isChecked()) {
            return;
        }

        cell.setChecked();

        if(cell.getShipInfoPointer() != null) {
            cell.getShipInfoPointer().decrementDecksAlive();

            if(cell.getShipInfoPointer().getDecksAlive() == 0) {
                this.markShipDestroyed(cell.getShipInfoPointer());
            }
        }

        this.rerenderCells();
        this.botClick();
    }

    public void rerenderCells() {
        Cell[][] cellsList = gamePane.getCells();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                Cell cell = cellsList[i][j];

                if(cellsList[i][j].getShipInfoPointer() != null && this.isUserPane) {
                    this.getChildren().get(j * 10 + i).getStyleClass().add("ship");
                }

                if(cell.isChecked()) {
                    if(cell.getShipInfoPointer() != null) {
                        this.getChildren().get(j * 10 + i).getStyleClass().add("penetrated");
                    } else {
                        this.getChildren().get(j * 10 + i).getStyleClass().add("missed");
                    }
                }
            }
        }
    }

    private void markShipDestroyed(ShipInfo shipInfo) {
        Cell[][] cellsList = gamePane.getCells();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                Cell cell = cellsList[i][j];

                if(cell.getShipInfoPointer() == shipInfo) {
                    for(int n = -1; n <= 1; n++) {
                        for(int m = -1; m <= 1; m++) {
                            int rowIndex = i + n;
                            int colIndex = j + m;
                            if(rowIndex >= 0 && rowIndex < 10 && colIndex >= 0 && colIndex < 10) {
                                cellsList[rowIndex][colIndex].setChecked();
                            }
                        }
                    }
                }
            }
        }

        rerenderCells();
    }

    private void botClick() {
        int row = rnd(10);
        int col = rnd(10);
        Cell cell = UserPane.getInstance().getCells()[row][col];

        if (cell.isChecked()) {
            botClick();
            return;
        }

        cell.setChecked();

        if(cell.getShipInfoPointer() != null) {
            cell.getShipInfoPointer().decrementDecksAlive();

            if(cell.getShipInfoPointer().getDecksAlive() == 0) {
                userPane.markShipDestroyed(cell.getShipInfoPointer());
            }
        }

        userPane.rerenderCells();
        this.checkForEndGame();
    }

    private int rnd(int max)
    {
        return (int) (Math.random() * max);
    }

    private void checkForEndGame() {

        boolean isBotFinished = true;
        boolean isUserFinished = true;

        for (Cell[] row : gamePane.getCells()) {
            for (Cell cell : row) {
                if (cell.getShipInfoPointer() != null && cell.getShipInfoPointer().getDecksAlive() > 0) {
                    isBotFinished = false;
                    break;
                }
            }
            if (!isBotFinished) {
                break;
            }
        }

        for (Cell[] row : UserPane.getInstance().getCells()) {
            for (Cell cell : row) {
                if (cell.getShipInfoPointer() != null && cell.getShipInfoPointer().getDecksAlive() > 0) {
                    isUserFinished = false;
                    break;
                }
            }
            if (!isUserFinished) {
                break;
            }
        }

        if (!isBotFinished && !isUserFinished) return;

        showAlertWindow();
    }

    private void showAlertWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Игра завершена");

        alert.setOnCloseRequest(e -> Platform.exit());

        alert.showAndWait();
    }
}
