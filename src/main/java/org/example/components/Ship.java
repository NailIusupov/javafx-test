package org.example.components;

import javafx.beans.NamedArg;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class Ship extends GridPane {

    private int decksCount;

    public Ship(
            @NamedArg("decksCount") int decksCount,
            @NamedArg("iconAlignment") String iconAlignment
    ) {

        this.decksCount = decksCount;

        if (iconAlignment.equals("vertical")) {
            for (int i = 0; i < decksCount; i++) {
                this.add(createShipDeck(), 0, i);
            }
        } else {
            for (int i = 0; i < decksCount; i++) {
                this.add(createShipDeck(), i, 0);
            }
        }
    }

    private Label createShipDeck() {
        Label deck = new Label();
        deck.getStyleClass().add("shipDeck");
        deck.setStyle("-fx-background-color: #ff715b; " +
                "-fx-border-color: #313640; " +
                "-fx-border-width: 1; " +
                "-fx-pref-width: 30; " +
                "-fx-pref-height: 30;");

        return deck;
    }

    public int getDecksCount() {
        return decksCount;
    }
}
