package org.example.components;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MainMenu extends MenuBar {

    public MainMenu() {

        Menu fileMenu = new Menu("File");
        MenuItem newItem = new MenuItem("New");

        fileMenu.getItems().add(newItem);

        this.getMenus().add(fileMenu);
    }
}
