package org.example.components;

import javafx.scene.layout.AnchorPane;

public class MainWrapper extends AnchorPane {

    public MainWrapper() {
        this.setStyle("-fx-background-color: #313640");
        this.minHeight(600);
        this.minWidth(800);

        MainMenu mainMenu = new MainMenu();

        AnchorPane.setLeftAnchor(mainMenu, 0.0);
        AnchorPane.setRightAnchor(mainMenu, 0.0);
        AnchorPane.setTopAnchor(mainMenu, 0.0);

        super.getChildren().add(mainMenu);
    }

}
