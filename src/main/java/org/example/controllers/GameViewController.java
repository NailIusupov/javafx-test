package org.example.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import org.example.App;
import org.example.components.BattlePane;
import org.example.components.MainWrapper;

import java.net.URL;
import java.util.ResourceBundle;

public class GameViewController implements Initializable {

    @FXML
    private MainWrapper mainWrapper;

    @FXML
    private BattlePane userPane;

    @FXML
    private BattlePane botPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainWrapper.getStylesheets().add(App.class.getResource("styles/secondary.css").toString());
        this.botPane.setUserPane(userPane);
    }
}
