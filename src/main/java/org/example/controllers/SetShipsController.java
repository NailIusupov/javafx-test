package org.example.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import org.example.App;
import org.example.components.MainWrapper;
import org.example.components.ShipsSettingPane;
import org.example.components.Ship;
import org.example.models.ShipInfo;
import org.example.models.UserPane;

public class SetShipsController implements Initializable {

    @FXML
    private MainWrapper mainWrapper;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainWrapper.getStylesheets().add(App.class.getResource("styles/secondary.css").toString());
        shipsSettingPane.selectedShipProperty().addListener((observable, oldValue, newValue) -> checkAndSetShip(newValue.getDecksCount(), newValue.getRow(), newValue.getColumn()));
    }

    private UserPane userPane = UserPane.getInstance();

    @FXML
    private Label oneDeckShip;

    @FXML
    private Label twoDeckShip;

    @FXML
    private Label threeDeckShip;

    @FXML
    private Label fourDeckShip;

    @FXML
    private ShipsSettingPane shipsSettingPane;

    @FXML
    private void finish() throws IOException {
        App.setRoot("game-view");
    }

    @FXML
    private void handleDragDetected(MouseEvent event) {
        Ship selectedShip = (Ship) event.getSource();

        if (selectedShip != null) {
            ClipboardContent content = new ClipboardContent();
            content.putString(String.valueOf(selectedShip.getDecksCount()));

            selectedShip.startDragAndDrop(TransferMode.ANY).setContent(content);
        }

        event.consume();
    }

    private void checkAndSetShip(int decksCount, int row, int column) {

        Label label = getLabel(decksCount);

        if (label == null) return;

        int currentCount = Integer.parseInt(label.getText());
        if (currentCount > 0) {
            setShip(decksCount, row, column);
            label.setText(String.valueOf((currentCount - 1)));
        }
    }

    private Label getLabel(int decksCount) {
        switch (decksCount) {
            case 1: {
                return oneDeckShip;
            }
            case 2: {
                return twoDeckShip;
            }
            case 3: {
                return threeDeckShip;
            }
            case 4: {
                return fourDeckShip;
            }
            default: return null;
        }
    }

    private void setShip(int decksCount, int row, int column) {
        ShipInfo ship = new ShipInfo(decksCount);

        for(int i = 0; i < decksCount; i++) {
            this.userPane.getCells()[row][column + i].setShipInfoPointer(ship);
        }
    }
}