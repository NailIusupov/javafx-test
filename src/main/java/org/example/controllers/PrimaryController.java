package org.example.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;
import org.example.App;

public class PrimaryController implements Initializable {

    @FXML
    private VBox rootWindowContainer;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rootWindowContainer.getStylesheets().add(App.class.getResource("styles/primary.css").toString());
    }

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }
}
