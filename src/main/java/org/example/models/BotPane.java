package org.example.models;

public class BotPane implements Pane {

    private Cell[][] cells = new Cell[10][10];

    public BotPane() {

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.cells[i][j] = new Cell();
            }
        }

        for (int j = 4; j >= 1; j--) {

            for (int k = 0; k < 4 - j + 1;) {
                ShipInfo ship = new ShipInfo(j);

                int row = rnd(10 - j);
                int col = rnd(10 - j);

                if (isCellsFree(row, col, j)) {
                    for (int i = 0; i < j; i++) {
                        this.cells[row][col + i].setShipInfoPointer(ship);
                    }
                    k++;
                }
            }
        }
    }

    public Cell[][] getCells() {
        return cells;
    }

    private int rnd(int max)
    {
        return (int) (Math.random() * max);
    }

    private boolean isCellsFree(int row, int col, int decksCount) {

        boolean isFree = true;

        for (int k = 0; k < decksCount; k++) {
            for (int n = -1; n <= 1; n++) {
                for (int m = -1; m <= 1; m++) {
                    int rowIndex = row + n;
                    int colIndex = col + m + k;

                    if (rowIndex >= 0 && rowIndex < 10 && colIndex >= 0 && colIndex < 10) {
                        if (this.cells[rowIndex][colIndex].getShipInfoPointer() != null) {
                            isFree = false;
                        }
                    }
                }
            }
        }

        return isFree;
    }
}
