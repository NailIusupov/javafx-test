package org.example.models;

public class SetShipActionModel {

    private int decksCount;
    private int row;
    private int column;

    public SetShipActionModel(int decksCount, int row, int column) {
        this.decksCount = decksCount;
        this.row = row;
        this.column = column;
    }

    public int getDecksCount() {
        return decksCount;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }
}
