package org.example.models;

public class Cell {

    private ShipInfo shipInfoPointer = null;
    private boolean checked = false;

    public ShipInfo getShipInfoPointer() {
        return shipInfoPointer;
    }

    public void setShipInfoPointer(ShipInfo shipInfoPointer) {
        this.shipInfoPointer = shipInfoPointer;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked() {
        this.checked = true;
    }
}
