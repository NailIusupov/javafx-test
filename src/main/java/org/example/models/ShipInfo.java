package org.example.models;

public class ShipInfo {

    private int decksCount;
    private int decksAlive;

    public ShipInfo(int decksCount) {
        this.decksCount = decksCount;
        this.decksAlive = decksCount;
    }

    public int getDecksCount() {
        return decksCount;
    }

    public void decrementDecksAlive() {
        this.decksAlive--;
    }

    public int getDecksAlive() {
        return decksAlive;
    }
}
