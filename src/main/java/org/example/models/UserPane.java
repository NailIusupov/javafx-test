package org.example.models;

public class UserPane implements Pane {

    private Cell[][] cells = new Cell[10][10];

    private UserPane() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.cells[i][j] = new Cell();
            }
        }
    }

    private static class PlaneHolder {
        private final static UserPane instance = new UserPane();
    }

    public static UserPane getInstance() {
        return PlaneHolder.instance;
    }

    public Cell[][] getCells() {
        return cells;
    }
}
