package org.example.models;

public interface Pane {

    Cell[][] getCells();
}
